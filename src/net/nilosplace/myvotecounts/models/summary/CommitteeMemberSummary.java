package net.nilosplace.myvotecounts.models.summary;

public class CommitteeMemberSummary {
 
	private int id;
	private CommitteeSummaryId committee;
	private PersonSummaryId person;
	private String role;
	private String role_label;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public CommitteeSummaryId getCommittee() {
		return committee;
	}
	public void setCommittee(CommitteeSummaryId committee) {
		this.committee = committee;
	}
	public PersonSummaryId getPerson() {
		return person;
	}
	public void setPerson(PersonSummaryId person) {
		this.person = person;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getRole_label() {
		return role_label;
	}
	public void setRole_label(String role_label) {
		this.role_label = role_label;
	}
}
