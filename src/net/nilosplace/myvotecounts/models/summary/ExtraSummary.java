package net.nilosplace.myvotecounts.models.summary;

public class ExtraSummary {

	private String address;
	private String contact_form;
	private String fax;
	private String office;
	private String rss_url;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContact_form() {
		return contact_form;
	}
	public void setContact_form(String contact_form) {
		this.contact_form = contact_form;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getOffice() {
		return office;
	}
	public void setOffice(String office) {
		this.office = office;
	}
	public String getRss_url() {
		return rss_url;
	}
	public void setRss_url(String rss_url) {
		this.rss_url = rss_url;
	}
}
