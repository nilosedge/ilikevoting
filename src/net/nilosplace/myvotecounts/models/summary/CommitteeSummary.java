package net.nilosplace.myvotecounts.models.summary;

public class CommitteeSummary {

	private int id;
	private CommitteeSummary committee;
	private String committee_type;
	private String code;
	private String abbrev;
	private String jurisdiction;
	private String jurisdiction_link;
	private String name;
	private Boolean obsolete;
	private String url;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public CommitteeSummary getCommittee() {
		return committee;
	}
	public void setCommittee(CommitteeSummary committee) {
		this.committee = committee;
	}
	public String getCommittee_type() {
		return committee_type;
	}
	public void setCommittee_type(String committee_type) {
		this.committee_type = committee_type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAbbrev() {
		return abbrev;
	}
	public void setAbbrev(String abbrev) {
		this.abbrev = abbrev;
	}
	public String getJurisdiction() {
		return jurisdiction;
	}
	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}
	public String getJurisdiction_link() {
		return jurisdiction_link;
	}
	public void setJurisdiction_link(String jurisdiction_link) {
		this.jurisdiction_link = jurisdiction_link;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isObsolete() {
		return obsolete;
	}
	public void setObsolete(boolean obsolete) {
		this.obsolete = obsolete;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}
