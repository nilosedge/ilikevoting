package net.nilosplace.myvotecounts.models.summary;

public class TermSummary {

	private int id;
	private String name;
	private String term_type;
	private String term_type_label;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTerm_type() {
		return term_type;
	}
	public void setTerm_type(String term_type) {
		this.term_type = term_type;
	}
	public String getTerm_type_label() {
		return term_type_label;
	}
	public void setTerm_type_label(String term_type_label) {
		this.term_type_label = term_type_label;
	}
	
	
}
