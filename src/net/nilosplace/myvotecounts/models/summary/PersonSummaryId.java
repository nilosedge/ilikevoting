package net.nilosplace.myvotecounts.models.summary;

public class PersonSummaryId {

	private int id;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
