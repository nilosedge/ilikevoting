package net.nilosplace.myvotecounts.models.summary;

import java.util.List;


public class BillSummary {

	private int id;
	
	private String bill_resolution_type;
	private String bill_type;
	private String bill_type_label;
	
	private int congress;
	private String current_status;
	private String current_status_date;
	private String current_status_description;
	private String current_status_label;
	private String display_number;
	private String docs_house_gov_postdate;
	private String introduced_date;
	private Boolean is_alive;
	private Boolean is_current;
	private String link;
	private Boolean lock_title;
	private String noun;
	private int number;
	private String senate_floor_schedule_postdate;
	private String sliplawnum;
	private String sliplawpubpriv;
	private String source;
	private String source_link;
	private String thomas_link;
	private String title;
	private String title_without_number;
	
	private List<CommitteeSummaryId> committees;
	private List<PersonSummaryId> cosponsors;
	private List<List<String>> major_actions;
	private PersonSummaryId sponsor;
	private RoleSummaryId sponsor_role;
	
	private List<TermSummary> terms;
	private List<List<String>> titles;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBill_resolution_type() {
		return bill_resolution_type;
	}
	public void setBill_resolution_type(String bill_resolution_type) {
		this.bill_resolution_type = bill_resolution_type;
	}
	public String getBill_type() {
		return bill_type;
	}
	public void setBill_type(String bill_type) {
		this.bill_type = bill_type;
	}
	public String getBill_type_label() {
		return bill_type_label;
	}
	public void setBill_type_label(String bill_type_label) {
		this.bill_type_label = bill_type_label;
	}
	public int getCongress() {
		return congress;
	}
	public void setCongress(int congress) {
		this.congress = congress;
	}
	public String getCurrent_status() {
		return current_status;
	}
	public void setCurrent_status(String current_status) {
		this.current_status = current_status;
	}
	public String getCurrent_status_date() {
		return current_status_date;
	}
	public void setCurrent_status_date(String current_status_date) {
		this.current_status_date = current_status_date;
	}
	public String getCurrent_status_description() {
		return current_status_description;
	}
	public void setCurrent_status_description(String current_status_description) {
		this.current_status_description = current_status_description;
	}
	public String getCurrent_status_label() {
		return current_status_label;
	}
	public void setCurrent_status_label(String current_status_label) {
		this.current_status_label = current_status_label;
	}
	public String getDisplay_number() {
		return display_number;
	}
	public void setDisplay_number(String display_number) {
		this.display_number = display_number;
	}
	public String getDocs_house_gov_postdate() {
		return docs_house_gov_postdate;
	}
	public void setDocs_house_gov_postdate(String docs_house_gov_postdate) {
		this.docs_house_gov_postdate = docs_house_gov_postdate;
	}
	public String getIntroduced_date() {
		return introduced_date;
	}
	public void setIntroduced_date(String introduced_date) {
		this.introduced_date = introduced_date;
	}
	public Boolean getIs_alive() {
		return is_alive;
	}
	public void setIs_alive(Boolean is_alive) {
		this.is_alive = is_alive;
	}
	public Boolean getIs_current() {
		return is_current;
	}
	public void setIs_current(Boolean is_current) {
		this.is_current = is_current;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public Boolean getLock_title() {
		return lock_title;
	}
	public void setLock_title(Boolean lock_title) {
		this.lock_title = lock_title;
	}
	public String getNoun() {
		return noun;
	}
	public void setNoun(String noun) {
		this.noun = noun;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getSenate_floor_schedule_postdate() {
		return senate_floor_schedule_postdate;
	}
	public void setSenate_floor_schedule_postdate(String senate_floor_schedule_postdate) {
		this.senate_floor_schedule_postdate = senate_floor_schedule_postdate;
	}
	public String getSliplawnum() {
		return sliplawnum;
	}
	public void setSliplawnum(String sliplawnum) {
		this.sliplawnum = sliplawnum;
	}
	public String getSliplawpubpriv() {
		return sliplawpubpriv;
	}
	public void setSliplawpubpriv(String sliplawpubpriv) {
		this.sliplawpubpriv = sliplawpubpriv;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getSource_link() {
		return source_link;
	}
	public void setSource_link(String source_link) {
		this.source_link = source_link;
	}
	public String getThomas_link() {
		return thomas_link;
	}
	public void setThomas_link(String thomas_link) {
		this.thomas_link = thomas_link;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle_without_number() {
		return title_without_number;
	}
	public void setTitle_without_number(String title_without_number) {
		this.title_without_number = title_without_number;
	}
	public List<CommitteeSummaryId> getCommittees() {
		return committees;
	}
	public void setCommittees(List<CommitteeSummaryId> committees) {
		this.committees = committees;
	}
	public List<PersonSummaryId> getCosponsors() {
		return cosponsors;
	}
	public void setCosponsors(List<PersonSummaryId> cosponsors) {
		this.cosponsors = cosponsors;
	}
	public List<List<String>> getMajor_actions() {
		return major_actions;
	}
	public void setMajor_actions(List<List<String>> major_actions) {
		this.major_actions = major_actions;
	}
	public PersonSummaryId getSponsor() {
		return sponsor;
	}
	public void setSponsor(PersonSummaryId sponsor) {
		this.sponsor = sponsor;
	}
	public RoleSummaryId getSponsor_role() {
		return sponsor_role;
	}
	public void setSponsor_role(RoleSummaryId sponsor_role) {
		this.sponsor_role = sponsor_role;
	}
	public List<TermSummary> getTerms() {
		return terms;
	}
	public void setTerms(List<TermSummary> terms) {
		this.terms = terms;
	}
	public List<List<String>> getTitles() {
		return titles;
	}
	public void setTitles(List<List<String>> titles) {
		this.titles = titles;
	}
}
