package net.nilosplace.myvotecounts.models.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import net.nilosplace.myvotecounts.models.summary.CommitteeSummary;

@Entity
@Table(name="committee")
public class Committee {
	
	@Id
	private int id;
	@ManyToOne
	private Committee committee;
	private String committee_type;
	private String code;
	private String abbrev;
	private String jurisdiction;
	private String jurisdiction_link;
	private String name;
	private Boolean obsolete;
	private String url;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "committee", cascade=CascadeType.ALL)
	private Set<PersonCommittee> personCommittee = new HashSet<PersonCommittee>();

	public Committee() {}
	
	public Committee(CommitteeSummary cs) {
		this.id = cs.getId();
		this.code = cs.getCode();
		this.abbrev = cs.getAbbrev();
		this.jurisdiction = cs.getJurisdiction();
		this.jurisdiction_link = cs.getJurisdiction_link();
		this.name = cs.getName();
		this.obsolete = cs.isObsolete();
		this.url = cs.getUrl();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Committee getCommittee() {
		return committee;
	}
	public void setCommittee(Committee committee) {
		this.committee = committee;
	}
	public String getCommittee_type() {
		return committee_type;
	}
	public void setCommittee_type(String committee_type) {
		this.committee_type = committee_type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAbbrev() {
		return abbrev;
	}
	public void setAbbrev(String abbrev) {
		this.abbrev = abbrev;
	}
	public String getJurisdiction() {
		return jurisdiction;
	}
	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}
	public String getJurisdiction_link() {
		return jurisdiction_link;
	}
	public void setJurisdiction_link(String jurisdiction_link) {
		this.jurisdiction_link = jurisdiction_link;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getObsolete() {
		return obsolete;
	}
	public void setObsolete(Boolean obsolete) {
		this.obsolete = obsolete;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	

	public Set<PersonCommittee> getPersonCommittee() {
		return personCommittee;
	}
	public void setPersonCommittee(Set<PersonCommittee> personCommittee) {
		this.personCommittee = personCommittee;
	}
}
