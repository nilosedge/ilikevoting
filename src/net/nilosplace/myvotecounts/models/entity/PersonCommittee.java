package net.nilosplace.myvotecounts.models.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import net.nilosplace.myvotecounts.models.summary.CommitteeMemberSummary;

@Entity
@Table(name="person_committee")
public class PersonCommittee {

	@Id
	private int id;

	@ManyToOne
	@JoinColumn(name="person_id")
	private Person person;
	@ManyToOne
	@JoinColumn(name="committee_id")
	private Committee committee;

	private String role;
	private String role_label;
	
	public PersonCommittee() {}
	
	public PersonCommittee(Person person, Committee committee, CommitteeMemberSummary cas) {
		this.id = cas.getId();
		this.person = person;
		this.committee = committee;
		this.role = cas.getRole();
		this.role_label = cas.getRole_label();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	
	public Committee getCommittee() {
		return committee;
	}
	public void setCommittee(Committee committee) {
		this.committee = committee;
	}
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getRole_label() {
		return role_label;
	}
	public void setRole_label(String role_label) {
		this.role_label = role_label;
	}
}
