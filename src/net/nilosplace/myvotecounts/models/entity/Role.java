package net.nilosplace.myvotecounts.models.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import net.nilosplace.myvotecounts.models.summary.RoleSummary;

@Entity
@Table(name="role")
public class Role {

	@Id
	private int id;
	@OneToOne
	private Person person;
	private String caucus;
	@OneToMany
	private List<CongressNumber> congress_numbers;
	private boolean current;
	private String description;
	private int district;
	private String enddate;
	@OneToOne
	private Extra extra;
	private String leadership_title;
	private String party;
	private String phone;
	private String role_type;
	private String role_type_label;
	private String senator_class;
	private String senator_rank;
	private String startdate;
	private String state;
	private String title;
	private String title_long;
	private String website;
	
	public Role() {}
	
	public Role(Person person, RoleSummary rs) {
		this.id = rs.getId();
		this.person = person;
		this.caucus = rs.getCaucus();
		this.current = rs.getCurrent();
		this.description = rs.getDescription();
		this.district = rs.getDistrict();
		this.enddate = rs.getEnddate();
		this.leadership_title = rs.getLeadership_title();
		this.party = rs.getParty();
		this.phone = rs.getPhone();
		this.role_type = rs.getRole_type();
		this.role_type_label = rs.getRole_type_label();
		this.senator_class = rs.getSenator_class();
		this.senator_rank = rs.getSenator_rank();
		this.startdate = rs.getStartdate();
		this.state = rs.getState();
		this.title = rs.getTitle();
		this.title_long = rs.getTitle_long();
		this.website = rs.getWebsite();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public String getCaucus() {
		return caucus;
	}
	public void setCaucus(String caucus) {
		this.caucus = caucus;
	}
	public List<CongressNumber> getCongress_numbers() {
		return congress_numbers;
	}
	public void setCongress_numbers(List<CongressNumber> congress_numbers) {
		this.congress_numbers = congress_numbers;
	}
	public boolean isCurrent() {
		return current;
	}
	public void setCurrent(boolean current) {
		this.current = current;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getDistrict() {
		return district;
	}
	public void setDistrict(int district) {
		this.district = district;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public Extra getExtra() {
		return extra;
	}
	public void setExtra(Extra extra) {
		this.extra = extra;
	}
	public String getLeadership_title() {
		return leadership_title;
	}
	public void setLeadership_title(String leadership_title) {
		this.leadership_title = leadership_title;
	}
	public String getParty() {
		return party;
	}
	public void setParty(String party) {
		this.party = party;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getRole_type() {
		return role_type;
	}
	public void setRole_type(String role_type) {
		this.role_type = role_type;
	}
	public String getRole_type_label() {
		return role_type_label;
	}
	public void setRole_type_label(String role_type_label) {
		this.role_type_label = role_type_label;
	}
	public String getSenator_class() {
		return senator_class;
	}
	public void setSenator_class(String senator_class) {
		this.senator_class = senator_class;
	}
	public String getSenator_rank() {
		return senator_rank;
	}
	public void setSenator_rank(String senator_rank) {
		this.senator_rank = senator_rank;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle_long() {
		return title_long;
	}
	public void setTitle_long(String title_long) {
		this.title_long = title_long;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
}
