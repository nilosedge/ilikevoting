package net.nilosplace.myvotecounts.models.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import net.nilosplace.myvotecounts.models.summary.PersonSummary;

@Entity
@Table(name="person")
public class Person {
	
	@Id
	private int id;
	private String bioguideid;
	private String birthday;
	private int cspanid;
	private String firstname;
	private String gender;
	private String gender_label;
	private String lastname;
	private String link;
	private String middlename;
	private String name;
	private String namemod;
	private String nickname;
	private String osid;
	private String pvsid;
	private String sortname;
	private String twitterid;
	private String youtubeid;

	@OneToMany(mappedBy="person")
	private List<Role> roles = new ArrayList<Role>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "person", cascade=CascadeType.ALL)
	private Set<PersonCommittee> personCommittee = new HashSet<PersonCommittee>();
	
	public Person() {}
	
	public Person(PersonSummary ps) {
		this.id = ps.getId();
		this.bioguideid = ps.getBioguideid();
		this.birthday = ps.getBirthday();
		this.cspanid = ps.getCspanid();
		this.firstname = ps.getFirstname();
		this.gender = ps.getGender();
		this.gender_label = ps.getGender_label();
		this.lastname = ps.getLastname();
		this.link = ps.getLink();
		this.middlename = ps.getMiddlename();
		this.name = ps.getName();
		this.namemod = ps.getNamemod();
		this.nickname = ps.getNickname();
		this.osid = ps.getOsid();
		this.pvsid = ps.getPvsid();
		this.sortname = ps.getSortname();
		this.twitterid = ps.getTwitterid();
		this.youtubeid = ps.getYoutubeid();
	}
	public String getBioguideid() {
		return bioguideid;
	}
	public void setBioguideid(String bioguideid) {
		this.bioguideid = bioguideid;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public int getCspanid() {
		return cspanid;
	}
	public void setCspanid(int cspanid) {
		this.cspanid = cspanid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getGender_label() {
		return gender_label;
	}
	public void setGender_label(String gender_label) {
		this.gender_label = gender_label;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNamemod() {
		return namemod;
	}
	public void setNamemod(String namemod) {
		this.namemod = namemod;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getOsid() {
		return osid;
	}
	public void setOsid(String osid) {
		this.osid = osid;
	}
	public String getPvsid() {
		return pvsid;
	}
	public void setPvsid(String pvsid) {
		this.pvsid = pvsid;
	}
	public String getSortname() {
		return sortname;
	}
	public void setSortname(String sortname) {
		this.sortname = sortname;
	}
	public String getTwitterid() {
		return twitterid;
	}
	public void setTwitterid(String twitterid) {
		this.twitterid = twitterid;
	}
	public String getYoutubeid() {
		return youtubeid;
	}
	public void setYoutubeid(String youtubeid) {
		this.youtubeid = youtubeid;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	public Set<PersonCommittee> getPersonCommittee() {
		return personCommittee;
	}
	public void setPersonCommittee(Set<PersonCommittee> personCommittee) {
		this.personCommittee = personCommittee;
	}
}
