package net.nilosplace.myvotecounts.models;

import java.util.List;

public class ObjectList<T> {

	private Meta meta;
	private List<T> objects;
	
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public List<T> getObjects() {
		return objects;
	}
	public void setObjects(List<T> objects) {
		this.objects = objects;
	}

}
