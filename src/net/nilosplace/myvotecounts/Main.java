package net.nilosplace.myvotecounts;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.PropertyConfigurator;

public class Main {

	//private static Logger log = Logger.getLogger(Main.class);
	
	public static void main(String[] args) {
		PropertyConfigurator.configure("log4j.properties");
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("MyVoteCounts");
		EntityManager em = emf.createEntityManager();
		
		
		//PersonManager pm = new PersonManager(em);
		
		//List<Person> list = pm.updatePersons();
		//List<Role> list = pm.updateRoles();
		//List<Committee> list = pm.updateCommittees();
		//List<PersonCommittee> list = pm.updatePersonCommittees();
		//List<Bill> list = pm.updateBills();

		em.close();
		emf.close();
		//org.hibernate.dialect.sql
	}

}
