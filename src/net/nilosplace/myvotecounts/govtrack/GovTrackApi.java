package net.nilosplace.myvotecounts.govtrack;

import java.util.ArrayList;
import java.util.List;

import net.nilosplace.myvotecounts.models.ObjectList;
import net.nilosplace.myvotecounts.models.summary.BillSummary;
import net.nilosplace.myvotecounts.models.summary.CommitteeMemberSummary;
import net.nilosplace.myvotecounts.models.summary.CommitteeSummary;
import net.nilosplace.myvotecounts.models.summary.PersonSummary;
import net.nilosplace.myvotecounts.models.summary.RoleSummary;

import org.apache.log4j.Logger;

import si.mazi.rescu.RestProxyFactory;

public class GovTrackApi {

	private Logger log = Logger.getLogger(getClass());
	private GovTrackApiInterface gti;
	
	
	public GovTrackApi() {
		gti = RestProxyFactory.createProxy(GovTrackApiInterface.class, "https://www.govtrack.us/api");
	}
	
	public List<PersonSummary> getPersons() {
		ArrayList<PersonSummary> ret = new ArrayList<PersonSummary>();
		ObjectList<PersonSummary> personList = null;
		int offset = 0;
		do {
			log.info("Grabbing Next Entries: " + offset);
			personList = gti.getPersonList(offset, 1000);
			ret.addAll(personList.getObjects());
			offset += personList.getMeta().getLimit();
		} while(offset < personList.getMeta().getTotal_count());
		return ret;
	}
	
	public List<RoleSummary> getRoles() {
		ArrayList<RoleSummary> ret = new ArrayList<RoleSummary>();
		ObjectList<RoleSummary> roleList = null;
		int offset = 0;
		do {
			log.info("Grabbing Next Entries: " + offset);
			roleList = gti.getRoleList(offset, offset + 6000, 6000);
			ret.addAll(roleList.getObjects());
			offset += 6000;
		} while(roleList.getMeta().getTotal_count() > 0);
		return ret;
	}
	
	public List<CommitteeSummary> getCommittees() {
		ArrayList<CommitteeSummary> ret = new ArrayList<CommitteeSummary>();
		ObjectList<CommitteeSummary> committeeList = null;
		int offset = 0;
		do {
			log.info("Grabbing Next Entries: " + offset);
			committeeList = gti.getCommitteeList(offset, 1000);
			ret.addAll(committeeList.getObjects());
			offset += committeeList.getMeta().getLimit();
		} while(offset < committeeList.getMeta().getTotal_count());
		return ret;
	}
	

	public List<CommitteeMemberSummary> getCommitteeMembers() {
		ArrayList<CommitteeMemberSummary> ret = new ArrayList<CommitteeMemberSummary>();
		ObjectList<CommitteeMemberSummary> committeeMemberList = null;
		int offset = 0;
		do {
			log.info("Grabbing Next Entries: " + offset);
			committeeMemberList = gti.getCommitteeMemberList(offset, 1000);
			ret.addAll(committeeMemberList.getObjects());
			offset += committeeMemberList.getMeta().getLimit();
		} while(offset < committeeMemberList.getMeta().getTotal_count());
		return ret;
	}
	
	public List<BillSummary> getBills() {
		ArrayList<BillSummary> ret = new ArrayList<BillSummary>();
		ObjectList<BillSummary> billList = null;
		int offset = 0;
		do {
			log.info("Grabbing Next Entries: " + offset);
			billList = gti.getBillList(offset, 3000);
			ret.addAll(billList.getObjects());
			offset += billList.getMeta().getLimit();
		} while(offset < billList.getMeta().getTotal_count());
		//} while(offset < 300);
		return ret;
	}
	
	public PersonSummary getPerson(int id) {
		return gti.getPerson(id);
	}

	public CommitteeSummary getCommittee(int id) {
		return gti.getCommittee(id);
	}

	public BillSummary getBill(int id) {
		return gti.getBill(id);
	}
	
}
