package net.nilosplace.myvotecounts.govtrack;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import net.nilosplace.myvotecounts.models.ObjectList;
import net.nilosplace.myvotecounts.models.summary.BillSummary;
import net.nilosplace.myvotecounts.models.summary.CommitteeMemberSummary;
import net.nilosplace.myvotecounts.models.summary.CommitteeSummary;
import net.nilosplace.myvotecounts.models.summary.PersonSummary;
import net.nilosplace.myvotecounts.models.summary.RoleSummary;

@Path("v2")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
public interface GovTrackApiInterface {

	@GET
	@Path("person")
	public ObjectList<PersonSummary> getPersonList(@QueryParam("offset") int offset, @QueryParam("limit") int limit);

	@GET
	@Path("role")
	public ObjectList<RoleSummary> getRoleList(@QueryParam("id__gte") int low, @QueryParam("id__lt") int high, @QueryParam("limit") int limit);
	
	@GET
	@Path("committee")
	public ObjectList<CommitteeSummary> getCommitteeList(@QueryParam("offset") int offset, @QueryParam("limit") int limit);
	
	@GET
	@Path("committee_member")
	public ObjectList<CommitteeMemberSummary> getCommitteeMemberList(@QueryParam("offset") int offset, @QueryParam("limit") int limit);
	
	@GET
	@Path("bill")
	public ObjectList<BillSummary> getBillList(@QueryParam("offset") int offset, @QueryParam("limit") int limit);

	
	@GET
	@Path("person/{personId}")
	public PersonSummary getPerson(@PathParam("personId") int personId);

	@GET
	@Path("committee/{committeeId}")
	public CommitteeSummary getCommittee(@PathParam("committeeId") int committeeId);

	@GET
	@Path("bill/{billId}")
	public BillSummary getBill(@PathParam("billId") int billId);

}
