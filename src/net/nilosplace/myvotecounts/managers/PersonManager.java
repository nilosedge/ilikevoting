package net.nilosplace.myvotecounts.managers;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import net.nilosplace.myvotecounts.govtrack.GovTrackApi;
import net.nilosplace.myvotecounts.models.entity.Bill;
import net.nilosplace.myvotecounts.models.entity.Committee;
import net.nilosplace.myvotecounts.models.entity.Person;
import net.nilosplace.myvotecounts.models.entity.PersonCommittee;
import net.nilosplace.myvotecounts.models.entity.Role;
import net.nilosplace.myvotecounts.models.entity.Term;
import net.nilosplace.myvotecounts.models.summary.BillSummary;
import net.nilosplace.myvotecounts.models.summary.CommitteeMemberSummary;
import net.nilosplace.myvotecounts.models.summary.CommitteeSummary;
import net.nilosplace.myvotecounts.models.summary.CommitteeSummaryId;
import net.nilosplace.myvotecounts.models.summary.PersonSummary;
import net.nilosplace.myvotecounts.models.summary.PersonSummaryId;
import net.nilosplace.myvotecounts.models.summary.RoleSummary;
import net.nilosplace.myvotecounts.models.summary.TermSummary;

import org.apache.log4j.Logger;

public class PersonManager {

	private EntityManager em;
	private GovTrackApi gta = new GovTrackApi();
	private Logger log = Logger.getLogger(getClass());

	public PersonManager(EntityManager em) {
		this.em = em;
	}

	public List<Person> updatePersons() {
		ArrayList<Person> ret = new ArrayList<Person>();
		List<PersonSummary> persons = gta.getPersons();
		log.info("Total Persons: " + persons.size());
		int count = 0;
		for(PersonSummary ps: persons) {

			Person person = new Person(ps);

			em.getTransaction().begin();
			em.merge(person);
			em.flush();
			em.getTransaction().commit();

			ret.add(person);
			log.info("Person: " + person.getName() + " " + count++);
		}
		return ret;
	}

	public List<Role> updateRoles() {
		ArrayList<Role> ret = new ArrayList<Role>();
		List<RoleSummary> roles = gta.getRoles();
		log.info("Total Roles: " + roles.size());

		int count = 0;
		em.getTransaction().begin();
		for(RoleSummary rs: roles) {
			Person person = readPerson(rs.getPerson().getId());
			Role role = new Role(person, rs);
			em.merge(role);
			ret.add(role);
			log.info("Role: " + role.getDescription() + " " + count++);
		}
		em.getTransaction().commit();
		return ret;
	}

	public List<Committee> updateCommittees() {
		ArrayList<Committee> ret = new ArrayList<Committee>();
		List<CommitteeSummary> committees = gta.getCommittees();
		log.info("Total Committees: " + committees.size());

		int count = 0;
		em.getTransaction().begin();
		for(CommitteeSummary cs: committees) {
			Committee committee = updateCommittee(cs.getId());
			ret.add(committee);
			log.info("Committee: " + committee.getJurisdiction() + " " + count++);
		}
		em.getTransaction().commit();
		return ret;
	}

	private Committee updateCommittee(int id) {

		CommitteeSummary cs = gta.getCommittee(id);
		Committee committee = new Committee(cs);
		if(cs.getCommittee() != null) {
			Committee c = updateCommittee(cs.getCommittee().getId());
			committee.setCommittee(c);
		}
		em.merge(committee);
		//em.flush();
		return committee;
	}
	
	public List<PersonCommittee> updatePersonCommittees() {
		ArrayList<PersonCommittee> ret = new ArrayList<PersonCommittee>();
		List<CommitteeMemberSummary> committeeMembers = gta.getCommitteeMembers();
		log.info("Total CommitteeMembers: " + committeeMembers.size());
		int count = 0;
		em.getTransaction().begin();
		for(CommitteeMemberSummary cs: committeeMembers) {
			Person person = readPerson(cs.getPerson().getId());
			Committee committee = readCommittee(cs.getCommittee().getId());
			PersonCommittee pc = new PersonCommittee(person, committee, cs);
			em.merge(pc);
			ret.add(pc);
			log.info("PersonCommittee: " + pc.getRole_label() + " " + count++);
		}
		em.getTransaction().commit();
		return ret;
	}
	
	public List<Bill> updateBills() {
		ArrayList<Bill> ret = new ArrayList<Bill>();
		List<BillSummary> bills = gta.getBills();
		log.info("Total Bills: " + bills.size());
		int count = 0;
		em.getTransaction().begin();
		for(BillSummary b: bills) {
			BillSummary bs = gta.getBill(b.getId());

			//private List<List<String>> major_actions;
			//private List<List<String>> titles;
			
			Bill bi = new Bill(b);
			
			ArrayList<Committee> committees = new ArrayList<Committee>();
			for(CommitteeSummaryId cs: bs.getCommittees()) {
				Committee committee = readCommittee(cs.getId());
				committees.add(committee);
			}
			bi.setCommittees(committees);
			
			ArrayList<Person> cosponsors = new ArrayList<Person>();
			for(PersonSummaryId ps: bs.getCosponsors()) {
				Person person = readPerson(ps.getId());
				cosponsors.add(person);
			}
			bi.setCoSponsors(cosponsors);
			
			ArrayList<Term> terms = new ArrayList<Term>();
			for(TermSummary ts: bs.getTerms()) {
				Term term = new Term(ts);
				terms.add(term);
			}
			bi.setTerms(terms);
			
			if(bs.getSponsor() != null) {
				Person sponsor = readPerson(bs.getSponsor().getId());
				bi.setSponsor(sponsor);
			}
			if(bs.getSponsor_role() != null) {
				Role sponsorRole = readRole(bs.getSponsor_role().getId());
				bi.setSponsorRole(sponsorRole);
			}
			
			em.merge(bi);
			ret.add(bi);
			log.info("Bill: " + b.getCurrent_status_description() + " " + count++);
		}
		em.getTransaction().commit();
		return ret;
		
	}

	public Role readRole(int id) {
		Role role = (Role)em.find(Role.class, id);
		return role;
	}

	public Person readPerson(int id) {
		Person person = (Person)em.find(Person.class, id);
		return person;
	}

	public Committee readCommittee(int id) {
		Committee committee = (Committee)em.find(Committee.class, id);
		return committee;
	}

	public PersonCommittee readPersonCommittee(int id) {
		PersonCommittee personCommittee = (PersonCommittee)em.find(PersonCommittee.class, id);
		return personCommittee;
	}

}
